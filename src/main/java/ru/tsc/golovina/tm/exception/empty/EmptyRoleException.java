package ru.tsc.golovina.tm.exception.empty;

import ru.tsc.golovina.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error. Role is empty.");
    }

}
