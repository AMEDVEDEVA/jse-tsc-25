package ru.tsc.golovina.tm.exception.system;

import ru.tsc.golovina.tm.exception.AbstractException;

public class UnknownRoleException extends AbstractException {

    public UnknownRoleException(final String role) {
        super("Error. Role `" + role + "` not found.");
    }

}
