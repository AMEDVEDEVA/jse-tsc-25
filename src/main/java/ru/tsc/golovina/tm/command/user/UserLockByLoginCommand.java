package ru.tsc.golovina.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class UserLockByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getCommand() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        @Nullable final String id = serviceLocator.getUserService().findUserByLogin(login).getId();
        @Nullable final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

}
